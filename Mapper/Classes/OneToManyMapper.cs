﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mapper.Classes
{
    public class OneToManyMapper : IOneToManyMapper
    {
        private const int MIN_VALID_IDENTIFIER = 1;
        private const int MAX_VALID_IDENTIFIER = 131072;
        private string rangeValidationMessage = $"Valid identifiers are in range {MIN_VALID_IDENTIFIER} and {MAX_VALID_IDENTIFIER}.";
        private string invalidRelationshipMessage = "You cannot link a parent with itself";

        public Dictionary<int, List<int>> BaseList { get; set; }

        public OneToManyMapper()
        {
            BaseList = new Dictionary<int, List<int>>();
        }

        public void Add(int parent, int child)
        {
            ValidateParameters(parent, child);

            // Validation to avoid mapping with itself
            if (parent == child)
                throw new InvalidOperationException(invalidRelationshipMessage);

            // Validation of the following business rule:
            // A child may have only one parent.
            int childsParent = GetParent(child);

            if (childsParent != 0)
                throw new Exception($"The child {child} has already a mapping with parent {childsParent}.");

            // Validation to avoid circular reference
            var parentTopLevel = GetParent(parent);

            do
            {
                if (parentTopLevel == child)
                    throw new Exception($"The parent {parent} has a circular reference with the child {child} and cannot be linked in the same mapping.");

                if (parentTopLevel != 0)
                    parentTopLevel = GetParent(parentTopLevel);
            } while (parentTopLevel != 0);

            if (BaseList.ContainsKey(parent))
                BaseList[parent].Add(child);
            else
                BaseList.Add(parent, new List<int>() { child });
        }

        public void RemoveChild(int child)
        {
            ValidateParameters(child: child);

            var parent = GetParent(child);

            if (BaseList.ContainsKey(parent))
            {
                var valuesListUpdated = BaseList[parent];
                valuesListUpdated.Remove(child);

                BaseList[parent] = valuesListUpdated;
            }
            else
                throw new Exception($"We could not find the child {child} to remove the mapping.");
        }

        public void RemoveParent(int parent)
        {
            ValidateParameters(parent: parent);

            if (BaseList.ContainsKey(parent))
                BaseList[parent] = new List<int>();
            else
                throw new Exception($"We could not find the parent {parent} to remove its mapping.");
        }

        public IEnumerable<int> GetChildren(int parent)
        {
            ValidateParameters(parent: parent);

            if (BaseList.ContainsKey(parent))
                return BaseList[parent];
            else
                return Enumerable.Empty<int>();
        }

        public int GetParent(int child)
        {
            ValidateParameters(child: child);

            var baseObject = BaseList.FirstOrDefault(k => k.Value.Contains(child));

            if (baseObject.Equals(default(Dictionary<int, List<int>>)))
                return 0;
            else
                return baseObject.Key;
        }

        private void ValidateParameters(int? parent = null, int? child = null)
        {
            if (parent != null)
                if (!IsValidIdentifier(parent.Value))
                    throw new ArgumentOutOfRangeException("parent", rangeValidationMessage);

            if (child != null)
                if (!IsValidIdentifier(child.Value))
                    throw new ArgumentOutOfRangeException("child", rangeValidationMessage);
        }

        /// <summary>
        /// Valid identifiers are in range [1..131072]
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns>Result if an identifier is valid</returns>
        private bool IsValidIdentifier(int identifier)
        {
            return identifier >= MIN_VALID_IDENTIFIER && identifier <= MAX_VALID_IDENTIFIER;
        }
    }
}