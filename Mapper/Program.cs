﻿using System;

namespace Mapper
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Implement the following interface. 
             * Optimise implementation for performance and minimal garbage collection.
             * Invalid operations (based on contract definition) should result in an exception.
             */

            Console.WriteLine("Welcome to Mapper");
            Console.WriteLine("Please run the project MapperTests to see the result of the tests.");
            
            Console.ReadLine();
        }
    }
}