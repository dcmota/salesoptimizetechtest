﻿using Mapper.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace MapperTests
{
    [TestClass]
    public class OneToManyMapperTests
    {
        [TestMethod]
        public void AddNewChildToNewParent()
        {
            OneToManyMapper oneToManyMapper = new OneToManyMapper();

            oneToManyMapper.Add(1, 2);

            var children = oneToManyMapper.GetChildren(1);

            Assert.AreEqual(2, children.FirstOrDefault());
        }

        [TestMethod]
        public void CheckChildrenUniquenessParentMapping_ShouldThrowException()
        {
            // A child may have only one parent
            OneToManyMapper oneToManyMapper = new OneToManyMapper();

            try
            {
                oneToManyMapper.Add(1, 2);
                oneToManyMapper.Add(5, 2);
            }
            catch (Exception e)
            {
                StringAssert.Contains(e.Message, "The child 2 has already a mapping with parent");
                return;
            }

            Assert.Fail("No exception was thrown.");
        }

        [TestMethod]
        public void CheckIfThereAreCircularReference_ShouldThrowException()
        {
            OneToManyMapper oneToManyMapper = new OneToManyMapper();

            try
            {
                oneToManyMapper.Add(1, 2);
                oneToManyMapper.Add(2, 3);
                oneToManyMapper.Add(3, 5);
                oneToManyMapper.Add(5, 1);
            }
            catch (Exception e)
            {
                StringAssert.Contains(e.Message, "The parent 5 has a circular reference with the child 1 and cannot be linked in the same mapping.");
                return;
            }

            Assert.Fail("No exception was thrown.");
        }

        [TestMethod]
        public void CheckInvalidIdentifiersRange_ShouldThrowArgumentOutOfRangeException()
        {
            OneToManyMapper oneToManyMapper = new OneToManyMapper();

            try
            {
                oneToManyMapper.Add(-1, 131075);
            }
            catch (ArgumentOutOfRangeException e)
            {
                StringAssert.Contains(e.Message, "Valid identifiers are in range 1 and 131072");
                return;
            }

            Assert.Fail("No exception was thrown.");
        }

        [TestMethod]
        public void CheckTheNumberOfChildrenInParent()
        {
            OneToManyMapper oneToManyMapper = new OneToManyMapper();

            oneToManyMapper.Add(1, 2);
            oneToManyMapper.Add(1, 3);
            oneToManyMapper.Add(1, 4);
            oneToManyMapper.Add(1, 5);
            oneToManyMapper.Add(1, 6);
            oneToManyMapper.Add(1, 7);

            var childrenList = oneToManyMapper.GetChildren(1);

            Assert.AreEqual(6, childrenList.Count());
        }

        [TestMethod]
        public void CheckIfThereIsInvalidMappingWithItself_ShouldThrowInvalidOperationException()
        {
            OneToManyMapper oneToManyMapper = new OneToManyMapper();

            try
            {
                oneToManyMapper.Add(1, 1);
            }
            catch (InvalidOperationException e)
            {
                StringAssert.Contains(e.Message, "You cannot link a parent with itself");
                return;
            }

            Assert.Fail("No exception was thrown.");
        }

        [TestMethod]
        public void CheckRemovingChildren()
        {
            OneToManyMapper oneToManyMapper = new OneToManyMapper();

            oneToManyMapper.Add(1, 5);
            oneToManyMapper.RemoveChild(5);

            Assert.AreEqual(0, oneToManyMapper.BaseList[1].Count);
        }

        [TestMethod]
        public void CheckRemovingChildrenWithoutMapping_ShouldThrowException()
        {
            OneToManyMapper oneToManyMapper = new OneToManyMapper();

            try
            {
                oneToManyMapper.RemoveChild(5);
            }
            catch (Exception e)
            {
                StringAssert.Contains(e.Message, $"We could not find the child 5 to remove the mapping.");
                return;
            }

            Assert.Fail("No exception was thrown.");
        }

        [TestMethod]
        public void CheckRemovingParent()
        {
            OneToManyMapper oneToManyMapper = new OneToManyMapper();

            oneToManyMapper.Add(1, 2);
            oneToManyMapper.Add(1, 3);
            oneToManyMapper.Add(1, 4);
            oneToManyMapper.Add(1, 5);

            oneToManyMapper.RemoveParent(1);

            Assert.AreEqual(0, oneToManyMapper.GetParent(5));
            Assert.AreEqual(0, oneToManyMapper.BaseList[1].Count);
        }

        [TestMethod]
        public void CheckRemovingNonExistentParent_ShouldThrowException()
        {
            OneToManyMapper oneToManyMapper = new OneToManyMapper();

            try
            {
                oneToManyMapper.RemoveParent(10);
            }
            catch (Exception e)
            {
                StringAssert.Contains(e.Message, "We could not find the parent 10 to remove its mapping.");
                return;
            }

            Assert.Fail("No exception was thrown.");
        }
    }
}