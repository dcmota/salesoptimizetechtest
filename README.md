# SalesOptimize Tech Test

## First problem: Mapper

To test the implementation of this interface, you will need to open and run the project called "MapperTests" and run the tests related to the project "Mapper".

---

## Second problem: PaperRulez

This project has a localdb file attached and it does not require any extra commands to start the application, but it needs to be on Visual Studio 2015+ and need to have a IIS Express enabled on the machine to be able to run as localhost since it is a web application.

---

**Note for the test:** I enjoyed a lot doing this test and I'm more than happy to explain my line of thought in each decision made for those projects.