﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaperRulez.Util
{
    public static class DocumentUtil
    {
        public static string CheckAndCreateCustomersFolder(string customerDirectory)
        {
            if (!System.IO.Directory.Exists(customerDirectory))
                System.IO.Directory.CreateDirectory(customerDirectory);

            return customerDirectory;
        }
    }
}