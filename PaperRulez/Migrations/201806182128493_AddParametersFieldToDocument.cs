namespace PaperRulez.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParametersFieldToDocument : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Document", "Parameters", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Document", "Parameters");
        }
    }
}
