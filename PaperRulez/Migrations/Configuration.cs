namespace PaperRulez.Migrations
{
    using PaperRulez.Models.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PaperRulez.Models.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PaperRulez.Models.DatabaseContext context)
        {
            if (context.Customers.Count() <= 0)
            {
                context.Customers.Add(new Customer() { Name = "Bank of Ireland" });
                context.Customers.Add(new Customer() { Name = "Braintree" });
                context.Customers.Add(new Customer() { Name = "Fexco" });
                context.Customers.Add(new Customer() { Name = "GLS" });
                context.Customers.Add(new Customer() { Name = "Interpay" });

                context.SaveChanges();
            }
        }
    }
}
