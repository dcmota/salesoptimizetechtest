namespace PaperRulez.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDocumentKeywordCountTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Document", "Customer_Id", "dbo.Customer");
            DropIndex("dbo.Document", new[] { "Customer_Id" });
            RenameColumn(table: "dbo.Document", name: "Customer_Id", newName: "CustomerID");
            CreateTable(
                "dbo.DocumentKeyword",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentID = c.Int(nullable: false),
                        Name = c.String(),
                        NumberOfRepetition = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Document", t => t.DocumentID, cascadeDelete: true)
                .Index(t => t.DocumentID);
            
            AlterColumn("dbo.Document", "CustomerID", c => c.Int(nullable: false));
            CreateIndex("dbo.Document", "CustomerID");
            AddForeignKey("dbo.Document", "CustomerID", "dbo.Customer", "Id", cascadeDelete: true);
            DropColumn("dbo.Document", "Parameters");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Document", "Parameters", c => c.String());
            DropForeignKey("dbo.Document", "CustomerID", "dbo.Customer");
            DropForeignKey("dbo.DocumentKeyword", "DocumentID", "dbo.Document");
            DropIndex("dbo.DocumentKeyword", new[] { "DocumentID" });
            DropIndex("dbo.Document", new[] { "CustomerID" });
            AlterColumn("dbo.Document", "CustomerID", c => c.Int());
            DropTable("dbo.DocumentKeyword");
            RenameColumn(table: "dbo.Document", name: "CustomerID", newName: "Customer_Id");
            CreateIndex("dbo.Document", "Customer_Id");
            AddForeignKey("dbo.Document", "Customer_Id", "dbo.Customer", "Id");
        }
    }
}
