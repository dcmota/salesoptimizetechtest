﻿using PaperRulez.Business;
using PaperRulez.Models;
using PaperRulez.Models.ViewModels;
using PaperRulez.Util;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace PaperRulez.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            CustomerViewModel customerVM = new CustomerViewModel();
            customerVM.CustomerSelectList = GetCustomerListItem();

            return View(customerVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SelectedCustomer(CustomerViewModel customer)
        {
            ProcessDocumentViewModel processDocument = new ProcessDocumentViewModel();
            processDocument.CustomerName = GetCustomerListItem().Find(s => s.Value.Equals(customer.SelectedCustomer)).Text;

            return RedirectToAction("ProcessDocument", processDocument);
        }

        public ActionResult ProcessDocument(ProcessDocumentViewModel documentViewModel)
        {
            return View(documentViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDocument(ProcessDocumentViewModel documentViewModel)
        {
            if (ModelState.IsValid)
            {
                string fullPathDocument = "";

                try
                {
                    // Only considering .txt and .csv files
                    if (documentViewModel.Document != null &&
                        documentViewModel.Document.ContentLength > 0 &&
                        ApplicationSettings.DOCUMENT_EXTENSION_UPLOAD.Contains(
                            documentViewModel.Document.FileName.Split('.')[documentViewModel.Document.FileName.Split('.').Length - 1]
                        ))
                    {
                        bool proccessed = false;
                        string customerVirtualDirectoryFiles = ApplicationSettings.CUSTOMER_VIRTUAL_DIRECTORY_PATH;

                        // Get document id from the file name
                        // Filename format: <document-id>_<document-name>.<content-type>
                        string documentID = documentViewModel.Document.FileName.Substring(0, documentViewModel.Document.FileName.IndexOf("_"));
                        customerVirtualDirectoryFiles += documentViewModel.CustomerName;

                        // Check if the customer's folder exists and create if it doesn't
                        string customerDirectory = DocumentUtil.CheckAndCreateCustomersFolder(Server.MapPath(customerVirtualDirectoryFiles));
                        fullPathDocument = customerDirectory + "/" + documentID + ".txt";

                        // Save the uploaded file in an internal folder
                        documentViewModel.Document.SaveAs(fullPathDocument);

                        using (var reader = new StreamReader(fullPathDocument))
                        {
                            var firstLine = reader.ReadLine();

                            // Read the first line to get which processing type to use for the document
                            string processingType = firstLine.Substring(0, firstLine.IndexOf("|")).Trim().ToLower();
                            string[] keywords = firstLine.Remove(0, firstLine.IndexOf("|") + 1).Trim().Split(',');

                            switch (processingType)
                            {
                                case "lookup":
                                    LookupDocument processor = new LookupDocument();
                                    // Set the content of the file
                                    processor.DocumentContent = reader.ReadToEnd();

                                    // Record the document information and keywords found in the file
                                    processor.Record(documentViewModel.CustomerName, documentID, keywords);
                                    proccessed = true;

                                    break;

                                default:
                                    break;
                            }
                        }

                        if (proccessed)
                        {
                            TempData["alert-class"] = "alert-success";
                            TempData["alert-message"] = "Document processed with success.";

                            return RedirectToAction("ProcessedDocument", documentViewModel);
                        }
                    }
                    else
                    {
                        TempData["alert-class"] = "alert-warning";
                        TempData["alert-message"] = "No document was processed. Extensions files allowed are only: .txt, .csv files.";

                        return RedirectToAction("ProcessDocument", documentViewModel);
                    }
                }
                catch (System.Exception e)
                {
                    // TODO: Save the exception in the database and/or send an email
                    TempData["alert-class"] = "alert-danger";
                    TempData["alert-message"] = "There was an unexpected error processing your document, please contact us or try again later.";

                    return RedirectToAction("");
                }
                finally
                {
                    if (!string.IsNullOrEmpty(fullPathDocument))
                        if (System.IO.File.Exists(fullPathDocument))
                            System.IO.File.Delete(fullPathDocument);
                }
            }

            TempData["alert-class"] = "alert-warning";
            TempData["alert-message"] = "No document was processed. Please review the document and try to upload again.";

            return RedirectToAction("ProcessDocument", documentViewModel);
        }

        public ActionResult ProcessedDocument(ProcessDocumentViewModel documentViewModel)
        {
            return View(documentViewModel);
        }

        private List<SelectListItem> GetCustomerListItem()
        {
            DatabaseContext dbContext = new DatabaseContext();

            var selectList = new List<SelectListItem>();

            var customers = dbContext.Customers;

            foreach (var customer in customers)
            {
                selectList.Add(new SelectListItem
                {
                    Value = customer.Id.ToString(),
                    Text = customer.Name
                });
            }

            return selectList;
        }
    }
}