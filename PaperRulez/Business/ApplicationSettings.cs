﻿namespace PaperRulez.Business
{
    public static class ApplicationSettings
    {
        public static string DOCUMENT_EXTENSION_UPLOAD = "txt, csv";
        public static string CUSTOMER_VIRTUAL_DIRECTORY_PATH = "~/Content/FileStorage/Customers/";
    }
}