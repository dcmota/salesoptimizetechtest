﻿using PaperRulez.Business.Interfaces;
using PaperRulez.Models;
using PaperRulez.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace PaperRulez.Business
{
    public class LookupDocument : ILookupStore
    {
        public string DocumentContent { get; set; }

        public void Record(string client, string documentId, IEnumerable<string> keywords)
        {
            try
            {
                DatabaseContext databaseContext = new DatabaseContext();

                var customer = databaseContext.Customers.FirstOrDefault(c => c.Name.Equals(client));

                if (customer != null)
                {
                    Document document = new Document();
                    document.Content = DocumentContent;
                    document.CustomerID = customer.Id;
                    document.Name = documentId;
                    document.Keywords = new List<DocumentKeyword>();

                    // Execute lookup search in the content of the document
                    MatchCollection matches = null;
                    foreach (var keyword in keywords)
                    {
                        matches = Regex.Matches(DocumentContent, keyword, RegexOptions.IgnoreCase);

                        if (matches != null && matches.Count > 0)
                        {
                            document.Keywords.Add(new DocumentKeyword()
                            {
                                Name = keyword,
                                NumberOfRepetition = matches.Count
                            });
                        }
                    }

                    databaseContext.Documents.Add(document);
                    databaseContext.SaveChanges();
                }
                else
                    throw new Exception("Customer not found");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}