﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaperRulez.Models.Entities
{
    [Table("Customer")]
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Document> Documents { get; set; }
    }
}