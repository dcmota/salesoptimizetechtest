﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaperRulez.Models.Entities
{
    [Table("Document")]
    public class Document
    {
        public int Id { get; set; }
        public int CustomerID { get; set; }

        [ForeignKey("CustomerID")]
        public Customer Customer { get; set; }

        public string Name { get; set; }
        public string Content { get; set; }
        public List<DocumentKeyword> Keywords { get; set; }
    }
}