﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PaperRulez.Models.Entities
{
    [Table("DocumentKeyword")]
    public class DocumentKeyword
    {
        public int Id { get; set; }

        public int DocumentID { get; set; }

        [ForeignKey("DocumentID")]
        public Document Document { get; set; }

        public string Name { get; set; }
        public int NumberOfRepetition { get; set; }
    }
}