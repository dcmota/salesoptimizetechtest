﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace PaperRulez.Models.ViewModels
{
    public class CustomerViewModel
    {
        public string CustomerName { get; set; }

        [Required]
        [Display(Name = "Select customer")]
        public string SelectedCustomer { get; set; }

        public List<SelectListItem> CustomerSelectList { get; set; }

        
    }
}