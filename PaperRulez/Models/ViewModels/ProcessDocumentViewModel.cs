﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PaperRulez.Models.ViewModels
{
    public class ProcessDocumentViewModel
    {
        [Required]
        public string CustomerName { get; set; }

        [Display(Name = "Select a document to process")]
        [UIHint("HttpPostedFileBase")]
        public HttpPostedFileBase Document { get; set; }
    }
}